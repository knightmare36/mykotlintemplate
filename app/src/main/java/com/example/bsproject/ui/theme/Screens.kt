package com.example.bsproject.ui.theme

sealed class Screen(val route : String) {

    object DataListScreen : Screen("data_list_screen")
}