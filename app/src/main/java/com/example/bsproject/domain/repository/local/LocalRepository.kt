package com.example.bsproject.domain.repository.local

import com.example.bsproject.data.model.DbModel

interface LocalRepository {
    suspend fun insertDataToDB(dbModel: DbModel)
    suspend fun getAllData() :  List<DbModel>
}