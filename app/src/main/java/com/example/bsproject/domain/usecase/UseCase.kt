package com.example.bsproject.domain.usecase

import com.example.bsproject.data.model.DbModel
import com.example.bsproject.data.model.ResponseData
import com.example.bsproject.domain.repository.RemoteRepository
import com.example.bsproject.domain.repository.local.LocalRepository

class UseCase(private val localRepository: LocalRepository, private val remoteRepository: RemoteRepository) {

    suspend fun getData() : ResponseData {
        return remoteRepository.getData()
    }

    suspend fun insertData(dbModel: DbModel) {
        return localRepository.insertDataToDB(dbModel)
    }
}