package com.example.bsproject.domain.repository

import com.example.bsproject.data.model.ResponseData

interface RemoteRepository {
    suspend fun getData() : ResponseData
}