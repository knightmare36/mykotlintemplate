package com.example.bsproject

import androidx.room.RoomDatabase
import com.example.bsproject.data.model.DbModel
import com.example.bsproject.data.repository.datasource.localdatasource.BsDao
@androidx.room.Database(
    entities = [DbModel::class],
    exportSchema = false,
    version = 1
)
abstract class BSDatabase : RoomDatabase() {
    abstract val bsDao: BsDao
    companion object {
        const val DATABASE_NAME = "bs_db"
    }
}