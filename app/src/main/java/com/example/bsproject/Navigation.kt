package com.example.bsproject

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.bsproject.presentation.components.landingscreen.DataListScreen
import com.example.bsproject.ui.theme.Screen

@Composable

fun Navigation(navController: NavController){

    NavHost(navController = navController as NavHostController, startDestination = Screen.DataListScreen.route){
      composable(route = Screen.DataListScreen.route){
          DataListScreen(navController = navController)
      }
    }
}