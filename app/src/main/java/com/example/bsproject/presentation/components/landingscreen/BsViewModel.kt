package com.example.bsproject.presentation.components.landingscreen

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bsproject.data.model.DbModel
import com.example.bsproject.domain.usecase.UseCase
import com.example.bsproject.presentation.components.landingscreen.states.MyDataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BsViewModel @Inject constructor(private val useCase: UseCase) : ViewModel() {
    private val _state = MutableStateFlow(MyDataState())
    val state = _state.asStateFlow()
    private var getDataJob: Job? = null

    var photoList = MutableLiveData<List<DbModel>>()

    init {
        getData()
    }

    fun insertData() {
        viewModelScope.launch(Dispatchers.IO) {
            useCase.insertData(DbModel(null))
        }
    }

    fun getData() {
        getDataJob?.cancel()
        getDataJob = viewModelScope.launch(Dispatchers.IO) {
            val dataList = useCase.getData()
            _state.value = _state.value.copy(
                dataList = dataList
            )
//            photoList.postValue(useCase.getData())
            Log.d("TAG", "getData: ${photoList.value}")
        }
    }
}