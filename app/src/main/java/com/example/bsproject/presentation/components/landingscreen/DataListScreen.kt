package com.example.bsproject.presentation.components.landingscreen

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.bsproject.data.model.ResponseDataItem

@Composable
fun DataListScreen(navController: NavController,
                   bsViewModel: BsViewModel = hiltViewModel()){
    val context = LocalContext.current
    val state by bsViewModel.state.collectAsState()

    var testVariable by remember {
        mutableStateOf("Hello")
    }

    MyList(state.dataList)
}

@Composable
fun MyList(items: List<ResponseDataItem>) {
    Spacer(modifier = Modifier.padding(25.dp))
    LazyColumn {
        items(items) { item ->
            Text(text = item.title)
            Spacer(modifier = Modifier.padding(5.dp))
            Text(text = item.id.toString())
        }
    }
    Spacer(modifier = Modifier.padding(25.dp))
}