package com.example.bsproject.presentation.components.landingscreen.states

import com.example.bsproject.data.model.ResponseDataItem

data class MyDataState(
    val dataList : List<ResponseDataItem> = emptyList()
)
