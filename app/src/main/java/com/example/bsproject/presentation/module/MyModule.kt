package com.example.bsproject.presentation.module

import android.app.Application
import androidx.room.Room
import com.example.bsproject.BSDatabase
import com.example.bsproject.data.api.ApiConstants
import com.example.bsproject.data.api.ApiService
import com.example.bsproject.data.repository.LocalRepositoryImpl
import com.example.bsproject.data.repository.RemoteRepositoryImpl
import com.example.bsproject.data.repository.datasource.DataSource
import com.example.bsproject.data.repository.datasourceimpl.DataSourceImpl
import com.example.bsproject.domain.repository.RemoteRepository
import com.example.bsproject.domain.repository.local.LocalRepository
import com.example.bsproject.domain.usecase.UseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
@Module
@InstallIn(SingletonComponent::class)
object MyModule {
    @Provides
    @Singleton
    fun provideNoteDatabase(app: Application): BSDatabase {
        return Room.databaseBuilder(app, BSDatabase::class.java, BSDatabase.DATABASE_NAME)
            .build()
    }
    @Provides
    @Singleton
    fun provideRemoteDataSource(apiService: ApiService): DataSource {
        return DataSourceImpl(apiService)
    }

    @Provides
    @Singleton
    fun provideApiRepository(dataSource: DataSource): RemoteRepository {
        return RemoteRepositoryImpl(dataSource)
    }

    @Provides
    @Singleton
    fun provideLocalRepository(presQDatabase: BSDatabase): LocalRepository {
        return LocalRepositoryImpl(presQDatabase.bsDao)
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .baseUrl(ApiConstants.BASE_URL)
            .build()
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideUseCase(
        localRepository: LocalRepository, remoteRepository: RemoteRepository): UseCase {
        return UseCase(localRepository, remoteRepository)
    }

}