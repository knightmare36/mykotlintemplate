package com.example.bsproject.data.repository.datasource

import com.example.bsproject.data.model.ResponseData

interface DataSource {
    suspend fun getDataList(): ResponseData
}