package com.example.bsproject.data.repository

import com.example.bsproject.data.model.DbModel
import com.example.bsproject.data.repository.datasource.localdatasource.BsDao
import com.example.bsproject.domain.repository.local.LocalRepository

class LocalRepositoryImpl(private val bsDao: BsDao) : LocalRepository {

    override suspend fun insertDataToDB(dbModel: DbModel) {
         bsDao.insertPets(dbModel)
    }

    override suspend fun getAllData(): List<DbModel> {
        return bsDao.getData()
    }
}