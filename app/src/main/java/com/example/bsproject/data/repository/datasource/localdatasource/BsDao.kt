package com.example.bsproject.data.repository.datasource.localdatasource

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.bsproject.data.model.DbModel

@Dao
interface BsDao {
    @Query("SELECT * FROM model")
    fun getData(): List<DbModel>

    @Query("SELECT * From model WHERE id = :id")
    suspend fun getPetsById(id:Int) : DbModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPets(dbModel: DbModel)
}