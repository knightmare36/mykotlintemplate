package com.example.bsproject.data.api

import com.example.bsproject.data.model.ResponseData
import retrofit2.http.GET

interface ApiService {
     @GET("todos")
     suspend fun getDataList():ResponseData
}