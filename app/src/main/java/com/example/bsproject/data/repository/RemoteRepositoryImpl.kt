package com.example.bsproject.data.repository

import com.example.bsproject.data.model.ResponseData
import com.example.bsproject.data.repository.datasource.DataSource
import com.example.bsproject.domain.repository.RemoteRepository


class RemoteRepositoryImpl(private val dataSource: DataSource) : RemoteRepository {
   override suspend fun getData(): ResponseData{
       return dataSource.getDataList()
    }
}