package com.example.bsproject.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "model")
data class DbModel(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    val name: String = "",
)
