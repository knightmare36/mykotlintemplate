package com.example.bsproject.data.model


import com.google.gson.annotations.SerializedName

data class ResponseDataItem(
    @SerializedName("completed")
    val completed: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String
)