package com.example.bsproject.data.repository.datasourceimpl

import com.example.bsproject.data.api.ApiService
import com.example.bsproject.data.model.ResponseData
import com.example.bsproject.data.repository.datasource.DataSource

class DataSourceImpl(private val apiService: ApiService) : DataSource {
    override suspend fun getDataList(): ResponseData{
        return apiService.getDataList()
    }
}