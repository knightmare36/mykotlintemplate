package com.example.bsproject

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BSApplication : Application() {
}